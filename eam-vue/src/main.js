import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementUI from 'element-ui';
import '../theme/index.css'
// import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';
import store from './store'
import router from './router'
Vue.use(ElementUI);
axios.defaults.baseURL = "http://localhost:8011/"
axios.defaults.allowedOrigins = true

// 将axios绑定给vue成为一个属性
Vue.prototype.axios = axios
Vue.use(VueAxios,axios)
Vue.config.productionTip = true;

new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app');
