import Router from 'vue-router'
import Vue from 'vue'
//test
// import test from './components/Page.vue'
// components
// import Home from './components/Home.vue'
// import ManagerPage from './components/ManagerPage.vue'
import Page from './components/Page.vue'
import Header from './components/Header.vue'
import sysAsider from './components/asides/sysAsider.vue'
import sysIndex from './components/indexs/sysIndex.vue'
// views
import write_order from './views/workorder/write_order.vue'
import adpart from './views/sysmanagers/adpart.vue'
import asset from './views/sysmanagers/asset.vue'
import prepaid_asset from './views/sysmanagers/prepaid_asset.vue'
//base_com
import borrow from './views/base_com/borrow.vue'
import basedata from './views/base_com/basedata.vue'
import purchase from './views/base_com/purchase.vue'
import store from './views/base_com/store.vue'
//basedate
import user from './views/basedata/user.vue'
import goods from './views/basedata/goods.vue'
import storehouse from './views/basedata/storehouse.vue'
import supplier from './views/basedata/supplier.vue'

//purchase
import order from './views/purchase/order.vue'
import warehousing from './views/purchase/warehousing.vue'

//store

import io from './views/store/io.vue'
import check from './views/store/check.vue'
import detail from './views/store/detail.vue'
import trans from './views/store/trans.vue'
//log
import log from './views/log/log.vue'

//borrow
import borrow_application from './views/borrow/borrow_application.vue'
import borrow_list from './views/borrow/borrow_list.vue'
import return_list from './views/borrow/return_list.vue'
//login
import Login from './views/login.vue'
Vue.use(Router)


export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path:'/login',
            component:Login
        },
        {
            path: '/',
            redirect: '/manager/basedata/user'
        },
        {
            path: '/manager',
            component: Page,
            children: [
                {
                    path: '',
                    components: {
                        asider: sysAsider,
                        default: sysIndex,
                        header: Header,
                    }
                },

                {
                    path: 'adpart',
                    components: {
                        asider: sysAsider,
                        default: adpart,
                        header: Header,

                    }
                },

                {
                    path: 'asset',
                    components: {
                        asider: sysAsider,
                        default: asset,
                        header: Header,

                    }
                }, {
                    path: 'log',
                    components: {
                        asider: sysAsider,
                        default: log,
                        header: Header,

                    }
                },
                
                {
                    path: 'prepaid_asset',
                    components: {
                        asider: sysAsider,
                        default: prepaid_asset,
                        header: Header,

                    }
                },
                {
                    path: 'write_order',
                    components: {
                        asider: sysAsider,
                        default: write_order,
                        header: Header,
                    }
                },
                {
                    path: 'basedata',
                    components: {
                        default: basedata,
                        header: Header,
                    },
                    children: [
                        {
                            path: 'user',
                            component: user
                        },
                        {
                            path: 'goods',
                            component: goods
                        },
                        {
                            path: 'storehouse',
                            component: storehouse
                        },
                        {
                            path: 'supplier',
                            component: supplier
                        },
                    ]
                },
                {
                    path: 'purchase',
                    components: {
                        asider: sysAsider,
                        header: Header,
                        default: purchase,
                    },
                    children: [
                        {
                            path: 'order',
                            component: order
                        },
                        {
                            path: 'warehousing',
                            component: warehousing
                        },
                    ]
                },
                {
                    path: 'borrow',
                    components: {
                        asider: sysAsider,
                        header: Header,
                        default: borrow,
                    },
                    children: [
                        {
                            path: 'borrow_application',
                            component: borrow_application
                        },
                        {
                            path: 'borrow_list',
                            component: borrow_list
                        },
                        {
                            path: 'return_list',
                            component: return_list
                        },
                    ]
                },
                {
                    path: 'store',
                    components: {
                        asider: sysAsider,
                        header: Header,
                        default: store,
                    },
                    children: [
                        {
                            path: 'io',
                            component: io
                        },
                        {
                            path: 'check',
                            component: check
                        },
                        {
                            path: 'detail',
                            component: detail
                        },
                        {
                            path: 'trans',
                            component: trans
                        },
                    ]
                },


            ]
        },
    ]
})