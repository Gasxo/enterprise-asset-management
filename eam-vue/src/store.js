import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


const state = {
  currentpath:'',
  collapsed: false,
  topNavState: 'home',
  leftNavState: 'home',
  isLogin:false,
  token:"",
  currentUser:null,
}

/*从本地存储读取数据*/
for(var item in state) {
  localStorage.getItem(item)? state[item] = JSON.parse(localStorage.getItem(item)): false;
}



export default new Vuex.Store({
  state,
  mutations: {
    storeSession (state,token) {
      // 变更状态
      state.token = token
    }
  }
})
