package com.app.eam.controller;

import com.app.eam.entity.Store;
import com.app.eam.service.StoreService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Store)表控制层
 *
 * @author makejava
 * @since 2020-03-29 19:18:27
 */
@RestController
@RequestMapping("store")
public class StoreController {
    /**
     * 服务对象
     */
    @Resource
    private StoreService storeService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Store selectOne(Integer id) {
        return this.storeService.queryById(id);
    }
    @PostMapping("insert")
    public String insert(@RequestBody Store store){
        this.storeService.insert(store);
        return "success";
    }
    @PostMapping("updata")
    public String updata(@RequestBody Store store){
        this.storeService.update(store);
        return "success";
    }
    @PostMapping("delete")
    public String delete(@RequestBody Store store){
        if(this.storeService.deleteById(store.getId())){
            return "success";
        }else{
            return "fail";
        }
    }
    @GetMapping("list")
    public List<Store> list(){
        return this.storeService.queryAll(new Store());
    }
}