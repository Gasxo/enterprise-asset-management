package com.app.eam.controller;

import com.app.eam.entity.BorrowDetail;
import com.app.eam.service.BorrowDetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (BorrowDetailDetail)表控制层
 *
 * @author makejava
 * @since 2020-04-02 17:02:50
 */
@RestController
@RequestMapping("BorrowDetailDetail")
public class BorrowDetailController {
    /**
     * 服务对象
     */
    @Resource
    private BorrowDetailService borrowDetailService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public BorrowDetail selectOne(Integer id) {
        return this.borrowDetailService.queryById(id);
    }

    @GetMapping("list")
    public List<BorrowDetail> list() {
        return this.borrowDetailService.queryAll(new BorrowDetail());
    }

    @PostMapping("delete")
    public String delete(@RequestBody BorrowDetail borrowDetail) {
        this.borrowDetailService.deleteByRefId(borrowDetail.getId());
        return "success";
    }

    @PostMapping("insert")
    public String insert(@RequestBody BorrowDetail[] list) {
        for (BorrowDetail borrowDetail : list) {
            this.borrowDetailService.insert(borrowDetail);
        }
        return "success";
    }

    @PostMapping("update")
    public String update(@RequestBody BorrowDetail borrowDetail) {
        this.borrowDetailService.update(borrowDetail);
        return "success";
    }

    @PostMapping("somelist")
    public List<BorrowDetail> somelist(@RequestBody BorrowDetail borrowDetail) {
        return this.borrowDetailService.queryAll(borrowDetail);
    }

}