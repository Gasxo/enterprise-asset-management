package com.app.eam.controller;

import com.app.eam.entity.Good;
import com.app.eam.entity.Supplier;
import com.app.eam.service.SupplierService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Supplier)表控制层
 *
 * @author makejava
 * @since 2020-03-29 23:25:16
 */
@RestController
@RequestMapping("supplier")
public class SupplierController {
    /**
     * 服务对象
     */
    @Resource
    private SupplierService supplierService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Supplier selectOne(Integer id) {
        return this.supplierService.queryById(id);
    }

    @GetMapping("list")
    public List<Supplier> list() {
        return this.supplierService.queryAll(new Supplier());
    }

    @PostMapping("somedata")
    public List<Supplier> list(@RequestBody Supplier supplier) {
        return this.supplierService.queryAll(supplier);
    }

    @PostMapping("insert")
    public String insert(@RequestBody Supplier supplier) {
        this.supplierService.insert(supplier);
        return "success";
    }
    @PostMapping("updata")
    public String updata(@RequestBody Supplier supplier) {
        this.supplierService.update(supplier);
        return "success";
    }
    @PostMapping("delete")
    public String delete(@RequestBody Supplier supplier) {
        this.supplierService.deleteById(supplier.getId());
        return "success";
    }
    @PostMapping("search")
    public List<Supplier> search(@RequestBody Supplier supplier){
        return this.supplierService.search(supplier);
    }
}