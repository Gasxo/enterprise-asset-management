package com.app.eam.controller;

import com.app.eam.entity.Storeborrow;
import com.app.eam.service.StoreborrowService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Storeborrow)表控制层
 *
 * @author makejava
 * @since 2020-04-04 14:11:20
 */
@RestController
@RequestMapping("storeborrow")
public class StoreborrowController {
    /**
     * 服务对象
     */
    @Resource
    private StoreborrowService storeborrowService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Storeborrow selectOne(Integer id) {

        return this.storeborrowService.queryById(id);
    }

    @GetMapping("list")
    public List<Storeborrow> list() {
        return this.storeborrowService.queryAll(new Storeborrow());
    }

    @PostMapping("update")
    public String update(@RequestBody Storeborrow borrow) {
        this.storeborrowService.update(borrow);
        return "success";
    }

    @PostMapping("insert")
    public String insert(@RequestBody Storeborrow borrow) {
        this.storeborrowService.insert(borrow);
        return "success";
    }
    @PostMapping("search")
    public  List<Storeborrow> search(@RequestBody Storeborrow borrow) {
        return  this.storeborrowService.search(borrow);
    }

}