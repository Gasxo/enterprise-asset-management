package com.app.eam.controller;

import com.app.eam.entity.AssetCheckDetail;
import com.app.eam.service.AssetCheckDetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetCheckDetail)表控制层
 *
 * @author makejava
 * @since 2020-04-11 16:59:56
 */
@RestController
@RequestMapping("assetCheckDetail")
public class AssetCheckDetailController {
    /**
     * 服务对象
     */
    @Resource
    private AssetCheckDetailService assetCheckDetailService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AssetCheckDetail selectOne(Integer id) {
        return this.assetCheckDetailService.queryById(id);
    }
    @PostMapping("list")
    public List<AssetCheckDetail> list(@RequestBody AssetCheckDetail assetCheckDetail){
        return this.assetCheckDetailService.queryAll(assetCheckDetail);
    }

    @PostMapping("insert")
    public String insert(@RequestBody AssetCheckDetail[] list){
        for (AssetCheckDetail assetCheckDetail : list) {
            this.assetCheckDetailService.insert(assetCheckDetail);
        }
        return "success";
    }
    @PostMapping("update")
    public String update(@RequestBody AssetCheckDetail[] list){
        for (AssetCheckDetail assetCheckDetail : list) {
            this.assetCheckDetailService.update(assetCheckDetail);
        }
        return "success";
    }
}