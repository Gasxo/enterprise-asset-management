package com.app.eam.controller;

import com.app.eam.entity.AssetIo;
import com.app.eam.service.AssetIoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetIo)表控制层
 *
 * @author makejava
 * @since 2020-04-11 10:35:33
 */
@RestController
@RequestMapping("assetIo")
public class AssetIoController {
    /**
     * 服务对象
     */
    @Resource
    private AssetIoService assetIoService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AssetIo selectOne(Integer id) {
        return this.assetIoService.queryById(id);
    }

    @PostMapping("list")
    public List<AssetIo> list(@RequestBody AssetIo assetIo){
        return this.assetIoService.queryAll(assetIo);
    }

}