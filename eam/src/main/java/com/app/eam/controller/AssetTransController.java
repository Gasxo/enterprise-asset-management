package com.app.eam.controller;

import com.app.eam.entity.AssetCheck;
import com.app.eam.entity.AssetTrans;
import com.app.eam.service.AssetTransDetailService;
import com.app.eam.service.AssetTransService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetTrans)表控制层
 *
 * @author makejava
 * @since 2020-04-11 12:53:47
 */
@RestController
@RequestMapping("assetTrans")
public class AssetTransController {
    /**
     * 服务对象
     */
    @Resource
    private AssetTransService assetTransService;
    @Resource
    private AssetTransDetailService assetTransDetailService;
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AssetTrans selectOne(Integer id) {
        return this.assetTransService.queryById(id);
    }
    @GetMapping("list")
    public List<AssetTrans> list() {
        return this.assetTransService.queryAll(new AssetTrans());
    }
    @PostMapping("insert")
    public String insert(@RequestBody AssetTrans assetTrans){
        return String.valueOf(this.assetTransService.insert(assetTrans).getId());
    }
    @PostMapping("update")
    public String update(@RequestBody AssetTrans assetTrans){
        return String.valueOf(this.assetTransService.update(assetTrans).getId());
    }
    @PostMapping("delete")
    public String delete(@RequestBody AssetTrans assetTrans){
        this.assetTransService.deleteById(assetTrans.getId());
        this.assetTransDetailService.deleteByTransId(assetTrans.getId());
        return "success";
    }
    @PostMapping("search")
    public List<AssetTrans> search(@RequestBody AssetTrans assetTrans){
        return this.assetTransService.search(assetTrans);

    }
}