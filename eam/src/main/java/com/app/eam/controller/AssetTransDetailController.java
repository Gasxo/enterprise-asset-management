package com.app.eam.controller;

import com.app.eam.entity.AssetCheckDetail;
import com.app.eam.entity.AssetTransDetail;
import com.app.eam.service.AssetTransDetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetTransDetail)表控制层
 *
 * @author makejava
 * @since 2020-04-15 18:29:56
 */
@RestController
@RequestMapping("assetTransDetail")
public class AssetTransDetailController {
    /**
     * 服务对象
     */
    @Resource
    private AssetTransDetailService assetTransDetailService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AssetTransDetail selectOne(Integer id) {
        return this.assetTransDetailService.queryById(id);
    }
    @PostMapping("list")
    public List<AssetTransDetail> list(@RequestBody AssetTransDetail assetTransDetail){
        return this.assetTransDetailService.queryAll(assetTransDetail);
    }
    @PostMapping("insert")
    public String insert(@RequestBody AssetTransDetail[] list){
        for (AssetTransDetail assetTransDetail : list) {
            this.assetTransDetailService.insert(assetTransDetail);
        }
        return "success";
    }
    @PostMapping("update")
    public String update(@RequestBody AssetTransDetail[] list){
        for (AssetTransDetail assetTransDetail : list) {
            this.assetTransDetailService.update(assetTransDetail);
        }
        return "success";
    }

}