package com.app.eam.controller;

import com.app.eam.entity.Good;
import com.app.eam.entity.GoodClass;
import com.app.eam.service.GoodClassService;
import com.app.eam.service.GoodService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (GoodClass)表控制层
 *
 * @author makejava
 * @since 2020-03-29 10:58:50
 */
@RestController
@RequestMapping("goodClass")
public class GoodClassController {
    /**
     * 服务对象
     */
    @Resource
    private GoodClassService goodClassService;
    @Resource
    private GoodService goodService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public GoodClass selectOne(Integer id) {
        return this.goodClassService.queryById(id);
    }

    @GetMapping("list")
    public List<GoodClass> list(){
        return this.goodClassService.treelist();
    }

    @PostMapping("insert")
    public String insert(@RequestBody GoodClass goodclass){
        this.goodClassService.insert(goodclass);
        return "success";
    }

    @PostMapping("fathername")
    public String fathername(@RequestBody GoodClass goodclass){
        return this.goodClassService.queryById(goodclass.getFather()).getName();
    }
    @PostMapping("classname")
    public String classname(@RequestBody GoodClass goodclass){
        return this.goodClassService.queryById(goodclass.getId()).getName();
    }
    @PostMapping("delete")
    public String delete(@RequestBody GoodClass goodclass){
        Good findGood = new Good();
        findGood.setClassid(goodclass.getId());
        GoodClass findObj = new GoodClass();
        findObj.setFather(goodclass.getId());
        if(this.goodService.queryAll(findGood).size()==0){
            if(this.goodClassService.queryAll(findObj).size()==0){
                this.goodClassService.deleteById(goodclass.getId());
                return "success";
            }else{
                return "havechildorg";
            }
        }else{
            return "havechild";
        }
    }

    @PostMapping("updata")
    public String Updata(@RequestBody GoodClass goodclass){
        this.goodClassService.update(goodclass);
        this.goodClassService.updateChild(goodclass);
        return "success";
    }
}