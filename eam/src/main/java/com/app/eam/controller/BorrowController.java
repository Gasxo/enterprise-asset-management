package com.app.eam.controller;

import com.app.eam.entity.Borrow;
import com.app.eam.service.BorrowService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Borrow)表控制层
 *
 * @author makejava
 * @since 2020-04-02 16:56:24
 */
@RestController
@RequestMapping("borrow")
public class BorrowController {
    /**
     * 服务对象
     */
    @Resource
    private BorrowService borrowService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Borrow selectOne(Integer id) {
        return this.borrowService.queryById(id);
    }

    @GetMapping("list")
    public List<Borrow> list(){
        return this.borrowService.queryAll(new Borrow());
    }
    @PostMapping("delete")
    public String delete(@RequestBody Borrow borrow){
        this.borrowService.deleteById(borrow.getId());
        return "success";
    }
    @PostMapping("insert")
    public String insert(@RequestBody Borrow borrow){
        return String.valueOf(this.borrowService.insert(borrow).getId());
    }
    @PostMapping("update")
    public String update(@RequestBody Borrow borrow){
        this.borrowService.update(borrow);
        return "success";
    }
    @PostMapping("search")
    public List<Borrow> search(@RequestBody Borrow borrow){
        return  this.borrowService.search(borrow);
    }

}