package com.app.eam.controller;

import com.app.eam.entity.User;
import com.app.eam.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2020-03-25 20:18:45
 */
@RestController
@RequestMapping("/user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param username 主键  id==username
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public User selectOne(String username) {
        System.out.println(username);
        return this.userService.queryById(username);
    }

    //    @CrossOrigin
    @PostMapping("getuserlist")
    public List<User> list(@RequestBody User user) {
        List<User> list = this.userService.queryAll(user);
        list.forEach(user1 -> {
            user1.setPassword(null);
        });
        return list;

    }

    @GetMapping("list")
    public List<User> list() {
        List<User> list = this.userService.queryAll(new User());
        list.forEach(user -> {
            user.setPassword(null);
        });
        return list;
    }

    @PostMapping("queryOne")
    public List<User> queryOne(@RequestBody User user) {
        return this.userService.queryAll(user);
    }

    @PostMapping("isAxist")
    public String isAxist(@RequestBody User user) {
//        User findobj = new User();
//        findobj.setUsername(user.getUsername());
        if (this.userService.queryAll(user).size() > 0)
            return "true";
        else
            return "false";
    }

    @PostMapping("updata")
    public String update(@RequestBody User user) {
        this.userService.update(user);
        return "success";
    }

    @PostMapping("insert")
    public String insert(@RequestBody User user) {
        this.userService.insert(user);
        return "success";
    }

    @PostMapping("delete")
    public String delete(@RequestBody User user) {
        if (this.userService.deleteById(user.getUsername()))
            return "success";
        else
            return "fail";
    }

    @PostMapping("search")
    public List<User> search(@RequestBody User user){
        return this.userService.search(user);
    }

}