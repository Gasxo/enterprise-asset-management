package com.app.eam.controller;

import com.app.eam.entity.AssetDetail;
import com.app.eam.entity.AssetIo;
import com.app.eam.service.AssetDetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetDetail)表控制层
 *
 * @author makejava
 * @since 2020-04-11 11:11:25
 */
@RestController
@RequestMapping("assetDetail")
public class AssetDetailController {
    /**
     * 服务对象
     */
    @Resource
    private AssetDetailService assetDetailService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AssetDetail selectOne(Integer id) {
        return this.assetDetailService.queryById(id);
    }

    @PostMapping("list")
    public List<AssetDetail> list(@RequestBody AssetDetail assetDetail) {
        return this.assetDetailService.queryAll(assetDetail);
    }

    @PostMapping("insert")
    public String insert(@RequestBody AssetDetail assetDetail) {
        AssetDetail findobj = new AssetDetail();
        //三个变量决定查询是否有库存
        findobj.setGoodcode(assetDetail.getGoodcode());
        findobj.setStatu(assetDetail.getStatu());
        findobj.setStoreid(assetDetail.getStoreid());
        List<AssetDetail> list = this.assetDetailService.queryAll(findobj);
        //没有库存
        if (list == null || list.size() == 0) {
            this.assetDetailService.insert(assetDetail);
        } else {//有库存
            findobj = list.get(0);
            findobj.setCount(findobj.getCount() + assetDetail.getCount());
            findobj.setZijin(findobj.getZijin() + assetDetail.getZijin());
            this.assetDetailService.update(findobj);
        }
        return "success";
    }

    @PostMapping("yanhuo")
    public String yanhuo(@RequestBody AssetDetail assetDetail) {
        AssetDetail findobj = new AssetDetail();
        findobj.setGoodcode(assetDetail.getGoodcode());
        findobj.setStatu(assetDetail.getStatu());
        findobj.setStoreid(assetDetail.getStoreid());
        List<AssetDetail> list = this.assetDetailService.queryAll(findobj);
        if (list == null || list.size() == 0) {
            this.assetDetailService.insert(assetDetail);
        } else {
            int num = list.get(0).getCount();
            AssetDetail original = list.get(0);
            if (assetDetail.getCount() >= num) {
                original.setZijin(assetDetail.getZijin());
                original.setCount(assetDetail.getCount());
                this.assetDetailService.update(original);
                return "success";
            }
            double price = assetDetail.getZijin() / assetDetail.getCount();
            assetDetail.setId(list.get(0).getId());
            this.assetDetailService.update(assetDetail);

            num -= assetDetail.getCount();
            assetDetail.setCount(num);
            assetDetail.setZijin(price * num);
            assetDetail.setStatu("报废");
            //查找报废是否存在
            findobj = new AssetDetail();
            findobj.setStatu("报废");
            findobj.setGoodcode(assetDetail.getGoodcode());
            findobj.setStoreid(assetDetail.getStoreid());
            list = this.assetDetailService.queryAll(findobj);
            if (list == null || list.size() == 0) {
                this.assetDetailService.insert(assetDetail);
            } else {
                findobj = list.get(0);
                findobj.setCount(findobj.getCount() + assetDetail.getCount());
                findobj.setZijin(findobj.getZijin() + assetDetail.getZijin());
                this.assetDetailService.update(findobj);
            }
        }
        return "success";
    }

    @PostMapping("check")
    public boolean check(@RequestBody AssetDetail[] list) {
        for (AssetDetail assetDetail : list) {
            AssetDetail findobj = new AssetDetail();
            findobj.setGoodcode(assetDetail.getGoodcode());
            findobj.setStatu("在库");
            findobj.setStoreid(assetDetail.getStoreid());
            List<AssetDetail> nowlist = this.assetDetailService.queryAll(findobj);
            if (nowlist == null || nowlist.size() == 0) {
                return false;
            } else if (nowlist.get(0).getCount() < assetDetail.getCount()) {
                return false;
            }
        }
        return true;
    }

    @PostMapping("borrow")
    public String borow(@RequestBody AssetDetail assetDetail) {
        AssetDetail findobj = new AssetDetail();
        findobj.setGoodcode(assetDetail.getGoodcode());
        findobj.setStatu("在库");
        findobj.setStoreid(assetDetail.getStoreid());
        List<AssetDetail> list = this.assetDetailService.queryAll(findobj);
        if (list == null || list.size() == 0) {
            return "no";
        } else {
            if (list.get(0).getCount() < assetDetail.getCount()) {
                return "no";
            }
            AssetDetail now = list.get(0);
            findobj.setStatu(assetDetail.getStatu());
            list = this.assetDetailService.queryAll(findobj);
            if (list == null || list.size() == 0) {
                this.assetDetailService.insert(assetDetail);
            } else {
                AssetDetail insert = list.get(0);
                insert.setZijin(assetDetail.getZijin() + insert.getZijin());
                insert.setCount(assetDetail.getCount() + insert.getCount());
                this.assetDetailService.update(insert);
            }
            now.setCount(now.getCount() - assetDetail.getCount());
            now.setZijin(now.getZijin() - assetDetail.getZijin());
            this.assetDetailService.update(now);
            return "success";
        }
    }

}