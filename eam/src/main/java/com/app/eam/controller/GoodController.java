package com.app.eam.controller;

import com.app.eam.entity.Good;
import com.app.eam.service.GoodService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Good)表控制层
 *
 * @author makejava
 * @since 2020-03-29 11:15:32
 */
@RestController
@RequestMapping("good")
public class GoodController {
    /**
     * 服务对象
     */
    @Resource
    private GoodService goodService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Good selectOne(Integer id) {
        return this.goodService.queryById(id);
    }
        @PostMapping("someGoodList")
    public List<Good> list(@RequestBody Good good) {
        return this.goodService.queryAll(good);
    }
    @PostMapping("updata")
    public String updata(@RequestBody Good good) {
        this.goodService.update(good);
        return "success";
    }
    @PostMapping("insert")
    public String insert(@RequestBody Good good) {
        this.goodService.insert(good);
        return "success";
    }
    @PostMapping("delete")
    public String delete(@RequestBody Good good) {
        if (this.goodService.deleteById(good.getId())){
            return "success";
        }else{
            return "fail";
        }
    }
    @PostMapping("getName")
    public String getname(@RequestBody Good good) {
        List<Good> list =  this.goodService.queryAll(good);
        if(list.size()==1){
            return list.get(0).getName();
        }
        return "fail";
    }

    @PostMapping("search")
    public List<Good> search(@RequestBody Good good){
        return this.goodService.search(good);
    }



}