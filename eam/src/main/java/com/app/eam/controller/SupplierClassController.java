package com.app.eam.controller;

import com.app.eam.entity.Supplier;
import com.app.eam.entity.SupplierClass;
import com.app.eam.service.SupplierClassService;
import com.app.eam.service.SupplierService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (SupplierClass)表控制层
 *
 * @author makejava
 * @since 2020-03-29 20:36:29
 */
@RestController
@RequestMapping("supplierClass")
public class SupplierClassController {
    /**
     * 服务对象
     */
    @Resource
    private SupplierClassService supplierClassService;
    @Resource
    private SupplierService supplierService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public SupplierClass selectOne(Integer id) {
        return this.supplierClassService.queryById(id);
    }

    @GetMapping("list")
    public List<SupplierClass> list() {
        List<SupplierClass> list = this.supplierClassService.queryAll(new SupplierClass());
        for (SupplierClass child : list) {
            Supplier findobj = new Supplier();
            findobj.setClassid(child.getId());
            child.setHave_num(supplierService.queryAll(findobj).size());
        }
        return list;
    }

    @PostMapping("insert")
    public String insert(@RequestBody SupplierClass supplierClass) {
        this.supplierClassService.insert(supplierClass);
        return "success";
    }
    @PostMapping("updata")
    public String updata(@RequestBody SupplierClass supplierClass) {
        this.supplierClassService.update(supplierClass);
        return "success";
    }
    @PostMapping("delete")
    public String delete(@RequestBody SupplierClass supplierClass) {
        this.supplierClassService.deleteById(supplierClass.getId());
        return "success";
    }

}