package com.app.eam.controller;

import com.app.eam.entity.AssetReturn;
import com.app.eam.service.AssetReturnService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetReturn)表控制层
 *
 * @author makejava
 * @since 2020-04-10 22:22:42
 */
@RestController
@RequestMapping("assetReturn")
public class AssetReturnController {
    /**
     * 服务对象
     */
    @Resource
    private AssetReturnService assetReturnService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AssetReturn selectOne(Integer id) {
        return this.assetReturnService.queryById(id);
    }

    @GetMapping("list")
    public List<AssetReturn> list(){
        return this.assetReturnService.queryAll(new AssetReturn());
    }

    @PostMapping("insert")
    public String insert(@RequestBody AssetReturn assetReturn){
        this.assetReturnService.insert(assetReturn);
        return "success";
    }

    @PostMapping("update")
    public String update(@RequestBody AssetReturn assetReturn){
        this.assetReturnService.update(assetReturn);

        return "success";
    }

    @PostMapping("delete")
    public String delete(@RequestBody AssetReturn assetReturn){
        this.assetReturnService.deleteById(assetReturn.getId());
        return "success";
    }
    @PostMapping("search")
    public List<AssetReturn> search(@RequestBody AssetReturn assetReturn){
        List<AssetReturn> list = this.assetReturnService.search(assetReturn);
        System.out.println(list.size());
        list.forEach(item -> {
            System.out.println(item.getReturnRef());
        });
        return list;
    }
}