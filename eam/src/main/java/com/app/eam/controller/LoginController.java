package com.app.eam.controller;

import com.app.eam.entity.Login;
import com.app.eam.entity.LoginVo;
import com.app.eam.entity.User;
import com.app.eam.service.LoginService;
import com.app.eam.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

/**
 * (Login)表控制层
 *
 * @author makejava
 * @since 2020-04-17 09:58:31
 */
@RestController
@RequestMapping("login")
public class LoginController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;
    @Resource
    private LoginService loginService;
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Login selectOne(Integer id) {
        return this.loginService.queryById(id);
    }
    //登录用户
    @PostMapping("login")
    public LoginVo login(@RequestBody User user){
        List<User> list = this.userService.queryAll(user);
        if(list==null || list.size()==0){
            //不存在该用户,返回"null"
            return null;
        }else if(list.size()==1){
            user.setId(list.get(0).getId());
            //存在该用户，则生成session缓存
            String thisUUID = UUID.randomUUID().toString();
            Login login = new Login();
            login.setUsername(list.get(0).getUsername());
            login.setName(list.get(0).getName());
            login.setLogintoken(thisUUID);
            Login login1 = new Login();
            login1.setUsername(user.getUsername());
            if(this.loginService.queryAll(login1).size()==0){
                this.loginService.insert(login);
            }else{
                this.loginService.update(login);
            }
            LoginVo vo = new LoginVo ();
            vo.setStatu("success");
            vo.setToken(thisUUID);
            return vo;
        }else{
            return null;
        }
    }
    //注销用户
    @GetMapping("logout")
    public String logout(@RequestParam("username") String username){
        //清除USER_SESSION_ID
        //清楚数据库的token,也是没有必要的，因为登录的时候，会刷新token
        //在检测用户是否登录时，会检验 token与存储在session的USER_SESSION_ID的对比，如果查到该USER_SESSION_ID,则会认定时该用户
//        User user = new User();
//        user.setId(Integer.valueOf(id));
//        user.setLogintoken("null");
//        this.userService.update(user);
        return "success";
    }

    //检验是否已登录状态,如果存在登录状态，则返回该用户
    @PostMapping("check")
    public Object check(@RequestBody LoginVo vo){
        if(vo.getToken()==null||vo.getToken()==""||vo.getToken()=="null"){
            return "no";
        }else{
            Login login = new Login();
            login.setLogintoken(vo.getToken());
            List<Login> list = this.loginService.queryAll(login);
            if(list==null||list.size()==0){//没有对应的token，估计是人为仿造session，应拒绝
                return null;
            }else if(list.size()==1){//存在，则会通知有一个用户在于登录状态
                Login myuser = list.get(0);
                myuser.setLogintoken(null);//防患下次登录再次使用该UUID
                return myuser;
            }else{
                return "error";
            }
        }
    }
    @GetMapping("look")
    public String look(HttpSession session){
        return String.valueOf(session.getId());
    }

}