package com.app.eam.controller;

import com.app.eam.entity.AssetCheck;
import com.app.eam.entity.AssetDetail;
import com.app.eam.entity.AssetReturn;
import com.app.eam.entity.Orderdetail;
import com.app.eam.service.AssetCheckDetailService;
import com.app.eam.service.AssetCheckService;
import com.app.eam.service.OrderdetailService;
import com.app.eam.service.impl.OrderdetailServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetCheck)表控制层
 *
 * @author makejava
 * @since 2020-04-11 12:36:02
 */
@RestController
@RequestMapping("assetCheck")
public class AssetCheckController {
    /**
     * 服务对象
     */
    @Resource
    private AssetCheckService assetCheckService;
    @Resource
    private AssetCheckDetailService assetCheckDetailService;
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public AssetCheck selectOne(Integer id) {
        return this.assetCheckService.queryById(id);
    }

    @GetMapping("list")
    public List<AssetCheck> list(){
        return this.assetCheckService.queryAll(new AssetCheck());
    }

    @PostMapping("delete")
    public String delete(@RequestBody AssetCheck assetCheck){
        this.assetCheckService.deleteById(assetCheck.getId());
        this.assetCheckDetailService.deleteBycheckId(assetCheck.getId());
        return "success";
    }
    @PostMapping("insert")
    public String insert(@RequestBody AssetCheck assetCheck){
        return String.valueOf(this.assetCheckService.insert(assetCheck).getId());
    }


    @PostMapping("update")
    public String update(@RequestBody AssetCheck assetCheck){
        return String.valueOf(this.assetCheckService.update(assetCheck).getId());
    }
    @PostMapping("search")
    public List<AssetCheck> search(@RequestBody AssetCheck assetCheck){
        return this.assetCheckService.search(assetCheck);

    }


}