package com.app.eam.controller;

import com.app.eam.entity.Order;
import com.app.eam.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Order)表控制层
 *
 * @author makejava
 * @since 2020-03-30 10:20:48
 */
@RestController
@RequestMapping("order")
public class OrderController {
    /**
     * 服务对象
     */
    @Resource
    private OrderService orderService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Order selectOne(Integer id) {
        return this.orderService.queryById(id);
    }

    @GetMapping("list")
    public List<Order> list() {
        return this.orderService.queryAll(new Order());
    }
    @PostMapping("search")
    public List<Order> list(@RequestBody Order order) {
        return this.orderService.search(order);
    }

    @PostMapping("insert")
    public String insert(@RequestBody Order order) {
        Order order1 = this.orderService.insert(order);
        return String.valueOf(order1.getId());
    }

    @PostMapping("updata")
    public String updata(@RequestBody Order order) {
        this.orderService.update(order);
        return "success";
    }

    @PostMapping("delete")
    public String delete(@RequestBody Order order) {
        this.orderService.deleteById(order.getId());
        return "success";
    }

    @PostMapping("finish")
    public String finish(@RequestBody Order order){
        order.setBillstatu("完成入库");
        this.updata(order);
        return "success";
    }

}