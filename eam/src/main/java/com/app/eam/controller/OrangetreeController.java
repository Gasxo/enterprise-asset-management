package com.app.eam.controller;

import com.app.eam.entity.Orangetree;
import com.app.eam.entity.User;
import com.app.eam.service.OrangetreeService;
import com.app.eam.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Orangetree)表控制层
 *
 * @author makejava
 * @since 2020-03-26 17:54:20
 */
@RestController
@RequestMapping("orangetree")
public class OrangetreeController {
    /**
     * 服务对象
     */
    @Resource
    private OrangetreeService orangetreeService;
    @Resource
    private UserService userService;
    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Orangetree selectOne(Integer id) {
        return this.orangetreeService.queryById(id);
    }

    @GetMapping("list")
    public List<Orangetree> list(){
        return this.orangetreeService.treelist();
    }

    @PostMapping("insert")
    public String insert(@RequestBody Orangetree orangetree){
        this.orangetreeService.insert(orangetree);
        return "success";
    }
    @PostMapping("fathername")
    public String fathername(@RequestBody Orangetree orangetree){
        return this.orangetreeService.queryById(orangetree.getFather()).getName();
    }
    @PostMapping("organename")
    public String organename(@RequestBody Orangetree orangetree){
        return this.orangetreeService.queryById(orangetree.getId()).getName();
    }
    @PostMapping("delete")
    public String delete(@RequestBody Orangetree orangetree){
        User finduser = new User();
        finduser.setOrangeid(orangetree.getId());
        Orangetree findorObj = new Orangetree();
        findorObj.setFather(orangetree.getId());
        if(this.userService.queryAll(finduser).size()==0){
            if(this.orangetreeService.queryAll(findorObj).size()==0){
                this.orangetreeService.deleteById(orangetree.getId());
                return "success";
            }else{
                return "havechildorg";
            }
        }else{
            return "havechild";
        }
    }

    @PostMapping("updata")
    public String Updata(@RequestBody Orangetree orangetree){

//        return String.valueOf(orangetree.getId());
        this.orangetreeService.update(orangetree);
        this.orangetreeService.updateChild(orangetree);
        return "success";
    }

}