package com.app.eam.controller;

import com.app.eam.entity.Orderdetail;
import com.app.eam.service.OrderdetailService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Orderdetail)表控制层
 *
 * @author makejava
 * @since 2020-03-31 15:18:14
 */
@RestController
@RequestMapping("orderdetail")
public class OrderdetailController {
    /**
     * 服务对象
     */
    @Resource
    private OrderdetailService orderdetailService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Orderdetail selectOne(Integer id) {
        return this.orderdetailService.queryById(id);
    }
    @PostMapping("somelist")
    public List<Orderdetail> somelist(@RequestBody  Orderdetail orderdetail){
        System.out.println(orderdetail.getOrderid());
        return this.orderdetailService.queryAll(orderdetail);
    }
    @PostMapping("insert")
    public String insert(@RequestBody Orderdetail[] list){
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("myarr");
        for (Orderdetail orderdetail : list) {
            this.orderdetailService.insert(orderdetail);
        }
        return "success";
    }
    @PostMapping("update")
    public String update(@RequestBody Orderdetail orderdetail){
        this.orderdetailService.update(orderdetail);
        return "success";
    }
    @PostMapping("delete")
    public String delete(@RequestBody Orderdetail orderdetail){
        this.orderdetailService.deleteByOrderid(orderdetail.getId());
        return "success";
    }
}