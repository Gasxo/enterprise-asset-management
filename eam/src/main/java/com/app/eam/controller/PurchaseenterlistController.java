package com.app.eam.controller;

import com.app.eam.entity.Purchaseenterlist;
import com.app.eam.service.PurchaseenterlistService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Purchaseenterlist)表控制层
 *
 * @author makejava
 * @since 2020-04-01 22:34:26
 */
@RestController
@RequestMapping("purchaseenterlist")
public class PurchaseenterlistController {
    /**
     * 服务对象
     */
    @Resource
    private PurchaseenterlistService purchaseenterlistService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Purchaseenterlist selectOne(Integer id) {
        return this.purchaseenterlistService.queryById(id);
    }

    @GetMapping("list")
    public List<Purchaseenterlist> list(){
        return this.purchaseenterlistService.queryAll(new Purchaseenterlist());
    }

    @PostMapping("insert")
    public String insert(@RequestBody Purchaseenterlist purchaseenterlist){
        this.purchaseenterlistService.insert(purchaseenterlist);
        return "success";
    }
    @PostMapping("update")
    public String update(@RequestBody Purchaseenterlist purchaseenterlist){
        this.purchaseenterlistService.update(purchaseenterlist);
        return "success";
    }


}