package com.app.eam.entity;

import java.io.Serializable;

/**
 * (AssetIo)实体类
 *
 * @author makejava
 * @since 2020-04-11 10:53:01
 */
public class AssetIo implements Serializable {
    private static final long serialVersionUID = -78907043813943777L;
    
    private Integer id;
    
    private Integer storeid;
    
    private String date;
    
    private String statu;
    
    private String ref;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStoreid() {
        return storeid;
    }

    public void setStoreid(Integer storeid) {
        this.storeid = storeid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

}