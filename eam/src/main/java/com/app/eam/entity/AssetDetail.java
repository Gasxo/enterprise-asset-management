package com.app.eam.entity;

import java.io.Serializable;

/**
 * (AssetDetail)实体类
 *
 * @author makejava
 * @since 2020-04-14 15:36:11
 */
public class AssetDetail implements Serializable {
    private static final long serialVersionUID = 104017672603586629L;
    
    private Integer id;
    
    private Integer storeid;
    
    private String goodcode;
    
    private String statu;
    
    private Double zijin;
    
    private String goodname;
    
    private Integer count;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStoreid() {
        return storeid;
    }

    public void setStoreid(Integer storeid) {
        this.storeid = storeid;
    }

    public String getGoodcode() {
        return goodcode;
    }

    public void setGoodcode(String goodcode) {
        this.goodcode = goodcode;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public Double getZijin() {
        return zijin;
    }

    public void setZijin(Double zijin) {
        this.zijin = zijin;
    }

    public String getGoodname() {
        return goodname;
    }

    public void setGoodname(String goodname) {
        this.goodname = goodname;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}