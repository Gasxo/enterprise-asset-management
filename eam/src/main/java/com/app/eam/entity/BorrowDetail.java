package com.app.eam.entity;

import java.io.Serializable;

/**
 * (BorrowDetail)实体类
 *
 * @author makejava
 * @since 2020-04-02 17:46:30
 */
public class BorrowDetail implements Serializable {
    private static final long serialVersionUID = -38350708599329943L;
    
    private Integer id;
    
    private Integer borrowref;
    
    private String code;
    
    private String name;
    
    private String spec;
    
    private Integer count;
    
    private String unitname;
    
    private String memo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBorrowref() {
        return borrowref;
    }

    public void setBorrowref(Integer borrowref) {
        this.borrowref = borrowref;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}