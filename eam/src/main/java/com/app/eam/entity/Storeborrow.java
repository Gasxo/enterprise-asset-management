package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Storeborrow)实体类
 *
 * @author makejava
 * @since 2020-04-04 15:59:10
 */
public class Storeborrow implements Serializable {
    private static final long serialVersionUID = -50291314403750519L;
    
    private Integer id;
    
    private String ref;
    
    private String statu;
    
    private String date;
    
    private String borrower;
    
    private String storeid;
    
    private String storename;
    
    private String bizname;
    
    private String memo;
    
    private String orangename;
    
    private String borrowref;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getBizname() {
        return bizname;
    }

    public void setBizname(String bizname) {
        this.bizname = bizname;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getOrangename() {
        return orangename;
    }

    public void setOrangename(String orangename) {
        this.orangename = orangename;
    }

    public String getBorrowref() {
        return borrowref;
    }

    public void setBorrowref(String borrowref) {
        this.borrowref = borrowref;
    }

}