package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Good)实体类
 *
 * @author makejava
 * @since 2020-04-14 13:52:01
 */
public class Good implements Serializable {
    private static final long serialVersionUID = 810833369575868093L;
    
    private Integer id;
    
    private String code;
    
    private String name;
    
    private Object saleprice;
    
    private String spec;
    
    private String unitname;
    
    private Object purchaseprice;
    
    private String brandfullname;
    
    private Object taxrate;
    
    private Integer classid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(Object saleprice) {
        this.saleprice = saleprice;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public Object getPurchaseprice() {
        return purchaseprice;
    }

    public void setPurchaseprice(Object purchaseprice) {
        this.purchaseprice = purchaseprice;
    }

    public String getBrandfullname() {
        return brandfullname;
    }

    public void setBrandfullname(String brandfullname) {
        this.brandfullname = brandfullname;
    }

    public Object getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(Object taxrate) {
        this.taxrate = taxrate;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

}