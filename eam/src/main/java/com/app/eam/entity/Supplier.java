package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Supplier)实体类
 *
 * @author makejava
 * @since 2020-03-29 23:25:16
 */
public class Supplier implements Serializable {
    private static final long serialVersionUID = -36905324488210678L;
    
    private Integer id;
    
    private String code;
    
    private String name;
    
    private String address;
    
    private String concact;
    
    private String phone;
    
    private String fixedline;
    
    private String qq;
    
    private String concact2;
    
    private String phone2;
    
    private String fixedline2;
    
    private Integer classid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConcact() {
        return concact;
    }

    public void setConcact(String concact) {
        this.concact = concact;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFixedline() {
        return fixedline;
    }

    public void setFixedline(String fixedline) {
        this.fixedline = fixedline;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getConcact2() {
        return concact2;
    }

    public void setConcact2(String concact2) {
        this.concact2 = concact2;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFixedline2() {
        return fixedline2;
    }

    public void setFixedline2(String fixedline2) {
        this.fixedline2 = fixedline2;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

}