package com.app.eam.entity;

import java.io.Serializable;

/**
 * (AssetTransDetail)实体类
 *
 * @author makejava
 * @since 2020-04-15 21:18:55
 */
public class AssetTransDetail implements Serializable {
    private static final long serialVersionUID = -40257871175954252L;
    
    private Integer id;
    
    private Integer transId;
    
    private String name;
    
    private String code;
    
    private Integer count;
    
    private String unitname;
    
    private String memo;
    
    private String spec;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTransId() {
        return transId;
    }

    public void setTransId(Integer transId) {
        this.transId = transId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

}