package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Orderdetail)实体类
 *
 * @author makejava
 * @since 2020-03-31 15:37:20
 */
public class Orderdetail implements Serializable {
    private static final long serialVersionUID = 418454246702203628L;
    
    private Integer id;
    
    private Integer orderid;
    
    private String code;
    
    private String name;
    
    private String spec;
    
    private String unitname;
    
    private Double taxrate;
    
    private Integer count;
    
    private Double price;
    
    private Double money;
    
    private Double moneywithtax;
    
    private Integer pwcount;
    
    private Integer leftcount;
    
    private String memo;
    
    private Double tax;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public Double getTaxrate() {
        return taxrate;
    }

    public void setTaxrate(Double taxrate) {
        this.taxrate = taxrate;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getMoneywithtax() {
        return moneywithtax;
    }

    public void setMoneywithtax(Double moneywithtax) {
        this.moneywithtax = moneywithtax;
    }

    public Integer getPwcount() {
        return pwcount;
    }

    public void setPwcount(Integer pwcount) {
        this.pwcount = pwcount;
    }

    public Integer getLeftcount() {
        return leftcount;
    }

    public void setLeftcount(Integer leftcount) {
        this.leftcount = leftcount;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

}