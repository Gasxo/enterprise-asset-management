package com.app.eam.entity;

import java.io.Serializable;
import java.util.List;

/**
 * (GoodClass)实体类
 *
 * @author makejava
 * @since 2020-03-29 10:58:45
 */
public class GoodClass implements Serializable {
    private static final long serialVersionUID = -37886521042505025L;
    
    private Integer id;
    
    private String name;
    
    private Integer level;
    
    private Integer father;
    
    private String code;

    private List<GoodClass> children;

    private String fathername;

    private int goodsnum;

    public int getGoodsnum() {
        return goodsnum;
    }

    public void setGoodsnum(int goodsnum) {
        this.goodsnum = goodsnum;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public List<GoodClass> getChildren() {
        return children;
    }

    public void setChildren(List<GoodClass> children) {
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getFather() {
        return father;
    }

    public void setFather(Integer father) {
        this.father = father;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}