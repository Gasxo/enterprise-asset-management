package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Borrow)实体类
 *
 * @author makejava
 * @since 2020-04-02 17:01:24
 */
public class Borrow implements Serializable {
    private static final long serialVersionUID = -62585013512231625L;
    
    private Integer id;
    
    private String ref;
    
    private String date;
    
    private String orangename;
    
    private String borrower;
    
    private String storeid;
    
    private String storename;
    
    private String bizName;
    
    private String memo;
    
    private String statu;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrangename() {
        return orangename;
    }

    public void setOrangename(String orangename) {
        this.orangename = orangename;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getBizName() {
        return bizName;
    }

    public void setBizName(String bizName) {
        this.bizName = bizName;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

}