package com.app.eam.entity;

import java.io.Serializable;

/**
 * (AssetTrans)实体类
 *
 * @author makejava
 * @since 2020-04-15 19:58:36
 */
public class AssetTrans implements Serializable {
    private static final long serialVersionUID = -77235605413736954L;
    
    private Integer id;
    
    private String storeI;
    
    private String storeO;
    
    private String bizname;
    
    private String date;
    
    private String memo;
    
    private String transRef;
    
    private String statu;
    
    private String storeIName;
    
    private String storeOName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStoreI() {
        return storeI;
    }

    public void setStoreI(String storeI) {
        this.storeI = storeI;
    }

    public String getStoreO() {
        return storeO;
    }

    public void setStoreO(String storeO) {
        this.storeO = storeO;
    }

    public String getBizname() {
        return bizname;
    }

    public void setBizname(String bizname) {
        this.bizname = bizname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getTransRef() {
        return transRef;
    }

    public void setTransRef(String transRef) {
        this.transRef = transRef;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getStoreIName() {
        return storeIName;
    }

    public void setStoreIName(String storeIName) {
        this.storeIName = storeIName;
    }

    public String getStoreOName() {
        return storeOName;
    }

    public void setStoreOName(String storeOName) {
        this.storeOName = storeOName;
    }

}