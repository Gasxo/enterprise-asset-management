package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Purchaseenterlist)实体类
 *
 * @author makejava
 * @since 2020-04-01 22:34:26
 */
public class Purchaseenterlist implements Serializable {
    private static final long serialVersionUID = -83056567191447146L;
    
    private Integer id;
    
    private String ref;
    
    private String dealdate;
    
    private String paymenttype;
    
    private String memo;
    
    private String bizusername;
    
    private String suppliername;
    
    private String orderid;
    
    private String storename;
    
    private String storeid;
    
    private String statu;
    
    private Double tax;
    
    private Double goodsmoney;
    
    private Double moneyandtax;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDealdate() {
        return dealdate;
    }

    public void setDealdate(String dealdate) {
        this.dealdate = dealdate;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getBizusername() {
        return bizusername;
    }

    public void setBizusername(String bizusername) {
        this.bizusername = bizusername;
    }

    public String getSuppliername() {
        return suppliername;
    }

    public void setSuppliername(String suppliername) {
        this.suppliername = suppliername;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getGoodsmoney() {
        return goodsmoney;
    }

    public void setGoodsmoney(Double goodsmoney) {
        this.goodsmoney = goodsmoney;
    }

    public Double getMoneyandtax() {
        return moneyandtax;
    }

    public void setMoneyandtax(Double moneyandtax) {
        this.moneyandtax = moneyandtax;
    }

}