package com.app.eam.entity;

import java.io.Serializable;

/**
 * (SupplierClass)实体类
 *
 * @author makejava
 * @since 2020-03-29 20:36:29
 */
public class SupplierClass implements Serializable {
    private static final long serialVersionUID = -65197559695371288L;
    
    private Integer id;
    
    private String code;
    
    private String name;

    private Integer have_num;

    public Integer getHave_num() {
        return have_num;
    }

    public void setHave_num(Integer have_num) {
        this.have_num = have_num;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}