package com.app.eam.entity;

import java.io.Serializable;

/**
 * (AssetCheck)实体类
 *
 * @author makejava
 * @since 2020-04-11 12:40:57
 */
public class AssetCheck implements Serializable {
    private static final long serialVersionUID = 441753167289669373L;
    
    private Integer id;
    
    private String typename;
    
    private Integer storeid;
    
    private String returnRef;
    
    private String checkRef;
    
    private String calRef;
    
    private String date;
    
    private String bizname;
    
    private String memo;
    
    private String statu;
    
    private String storename;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public Integer getStoreid() {
        return storeid;
    }

    public void setStoreid(Integer storeid) {
        this.storeid = storeid;
    }

    public String getReturnRef() {
        return returnRef;
    }

    public void setReturnRef(String returnRef) {
        this.returnRef = returnRef;
    }

    public String getCheckRef() {
        return checkRef;
    }

    public void setCheckRef(String checkRef) {
        this.checkRef = checkRef;
    }

    public String getCalRef() {
        return calRef;
    }

    public void setCalRef(String calRef) {
        this.calRef = calRef;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBizname() {
        return bizname;
    }

    public void setBizname(String bizname) {
        this.bizname = bizname;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

}