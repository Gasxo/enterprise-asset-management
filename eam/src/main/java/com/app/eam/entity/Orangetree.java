package com.app.eam.entity;

import java.io.Serializable;
import java.util.List;

/**
 * (Orangetree)实体类
 *
 * @author makejava
 * @since 2020-03-26 17:54:13
 */
public class Orangetree implements Serializable {
    private static final long serialVersionUID = -92099414382567682L;
    
    private Integer id;
    
    private String name;
    
    private Integer level;
    
    private Integer father;
    
    private Integer usernum;

    private String code;

    private String fathername;
    private List<Orangetree> children;


    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Orangetree> getChildren() {
        return children;
    }

    public void setChildren(List<Orangetree> children) {
        this.children = children;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getFather() {
        return father;
    }

    public void setFather(Integer father) {
        this.father = father;
    }

    public Integer getUsernum() {
        return usernum;
    }

    public void setUsernum(Integer usernum) {
        this.usernum = usernum;
    }

}