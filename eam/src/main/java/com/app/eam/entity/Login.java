package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Login)实体类
 *
 * @author makejava
 * @since 2020-04-17 09:58:31
 */
public class Login implements Serializable {
    private static final long serialVersionUID = 436753385395006298L;
    
    private Integer id;
    
    private String username;
    
    private String name;
    
    private String logintoken;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogintoken() {
        return logintoken;
    }

    public void setLogintoken(String logintoken) {
        this.logintoken = logintoken;
    }

}