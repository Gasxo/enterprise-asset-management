package com.app.eam.entity;

import java.io.Serializable;

/**
 * (AssetReturn)实体类
 *
 * @author makejava
 * @since 2020-04-17 20:43:57
 */
public class AssetReturn implements Serializable {
    private static final long serialVersionUID = -53729262089359776L;
    
    private Integer id;
    
    private String statu;
    
    private String isappointment;
    
    private String appDate;
    
    private String borrowId;
    
    private String returnDate;
    
    private String borrowername;
    
    private String barrowerOrgname;
    
    private String bizname;
    
    private String returnRef;
    
    private String borrowcode;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getIsappointment() {
        return isappointment;
    }

    public void setIsappointment(String isappointment) {
        this.isappointment = isappointment;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getBorrowId() {
        return borrowId;
    }

    public void setBorrowId(String borrowId) {
        this.borrowId = borrowId;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getBorrowername() {
        return borrowername;
    }

    public void setBorrowername(String borrowername) {
        this.borrowername = borrowername;
    }

    public String getBarrowerOrgname() {
        return barrowerOrgname;
    }

    public void setBarrowerOrgname(String barrowerOrgname) {
        this.barrowerOrgname = barrowerOrgname;
    }

    public String getBizname() {
        return bizname;
    }

    public void setBizname(String bizname) {
        this.bizname = bizname;
    }

    public String getReturnRef() {
        return returnRef;
    }

    public void setReturnRef(String returnRef) {
        this.returnRef = returnRef;
    }

    public String getBorrowcode() {
        return borrowcode;
    }

    public void setBorrowcode(String borrowcode) {
        this.borrowcode = borrowcode;
    }

}