package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Store)实体类
 *
 * @author makejava
 * @since 2020-03-31 14:55:12
 */
public class Store implements Serializable {
    private static final long serialVersionUID = -65775590245983838L;
    
    private Integer id;
    
    private String name;
    
    private String address;
    
    private String orangeid;
    
    private String orangename;
    
    private String code;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrangeid() {
        return orangeid;
    }

    public void setOrangeid(String orangeid) {
        this.orangeid = orangeid;
    }

    public String getOrangename() {
        return orangename;
    }

    public void setOrangename(String orangename) {
        this.orangename = orangename;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}