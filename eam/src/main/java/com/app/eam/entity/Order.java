package com.app.eam.entity;

import java.io.Serializable;

/**
 * (Order)实体类
 *
 * @author makejava
 * @since 2020-04-01 13:31:33
 */
public class Order implements Serializable {
    private static final long serialVersionUID = -56622408336835046L;
    
    private Integer id;
    
    private String ref;
    
    private String billstatu;
    
    private String dealdate;
    
    private String dealaddress;
    
    private Integer suppliserid;
    
    private Double goodsmoney;
    
    private Double tax;
    
    private Double moneyandtax;
    
    private String paymenttype;
    
    private String memo;
    
    private String bizusername;
    
    private String orgname;
    
    private String contact;
    
    private String suppliername;
    
    private String inputusername;
    
    private String datecreated;
    
    private String confirmusername;
    
    private String confirmdate;
    
    private String phone;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getBillstatu() {
        return billstatu;
    }

    public void setBillstatu(String billstatu) {
        this.billstatu = billstatu;
    }

    public String getDealdate() {
        return dealdate;
    }

    public void setDealdate(String dealdate) {
        this.dealdate = dealdate;
    }

    public String getDealaddress() {
        return dealaddress;
    }

    public void setDealaddress(String dealaddress) {
        this.dealaddress = dealaddress;
    }

    public Integer getSuppliserid() {
        return suppliserid;
    }

    public void setSuppliserid(Integer suppliserid) {
        this.suppliserid = suppliserid;
    }

    public Double getGoodsmoney() {
        return goodsmoney;
    }

    public void setGoodsmoney(Double goodsmoney) {
        this.goodsmoney = goodsmoney;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getMoneyandtax() {
        return moneyandtax;
    }

    public void setMoneyandtax(Double moneyandtax) {
        this.moneyandtax = moneyandtax;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getBizusername() {
        return bizusername;
    }

    public void setBizusername(String bizusername) {
        this.bizusername = bizusername;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getSuppliername() {
        return suppliername;
    }

    public void setSuppliername(String suppliername) {
        this.suppliername = suppliername;
    }

    public String getInputusername() {
        return inputusername;
    }

    public void setInputusername(String inputusername) {
        this.inputusername = inputusername;
    }

    public String getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(String datecreated) {
        this.datecreated = datecreated;
    }

    public String getConfirmusername() {
        return confirmusername;
    }

    public void setConfirmusername(String confirmusername) {
        this.confirmusername = confirmusername;
    }

    public String getConfirmdate() {
        return confirmdate;
    }

    public void setConfirmdate(String confirmdate) {
        this.confirmdate = confirmdate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}