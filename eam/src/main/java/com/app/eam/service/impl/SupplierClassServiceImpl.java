package com.app.eam.service.impl;

import com.app.eam.dao.SupplierClassDao;
import com.app.eam.entity.SupplierClass;
import com.app.eam.service.SupplierClassService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (SupplierClass)表服务实现类
 *
 * @author makejava
 * @since 2020-03-29 20:36:29
 */
@Service("supplierClassService")
public class SupplierClassServiceImpl implements SupplierClassService {
    @Resource
    private SupplierClassDao supplierClassDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public SupplierClass queryById(Integer id) {
        return this.supplierClassDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<SupplierClass> queryAllByLimit(int offset, int limit) {
        return this.supplierClassDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param supplierClass 实例对象
     * @return 实例对象
     */
    @Override
    public SupplierClass insert(SupplierClass supplierClass) {
        this.supplierClassDao.insert(supplierClass);
        return supplierClass;
    }

    /**
     * 修改数据
     *
     * @param supplierClass 实例对象
     * @return 实例对象
     */
    @Override
    public SupplierClass update(SupplierClass supplierClass) {
        this.supplierClassDao.update(supplierClass);
        return this.queryById(supplierClass.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.supplierClassDao.deleteById(id) > 0;
    }

    @Override
    public List<SupplierClass> queryAll(SupplierClass supplierClass) {
        return this.supplierClassDao.queryAll(supplierClass);
    }
}