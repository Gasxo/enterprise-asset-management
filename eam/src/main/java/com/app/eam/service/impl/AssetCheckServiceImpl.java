package com.app.eam.service.impl;

import com.app.eam.entity.AssetCheck;
import com.app.eam.dao.AssetCheckDao;
import com.app.eam.service.AssetCheckService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetCheck)表服务实现类
 *
 * @author makejava
 * @since 2020-04-11 12:36:02
 */
@Service("assetCheckService")
public class AssetCheckServiceImpl implements AssetCheckService {
    @Resource
    private AssetCheckDao assetCheckDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssetCheck queryById(Integer id) {
        return this.assetCheckDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<AssetCheck> queryAllByLimit(int offset, int limit) {
        return this.assetCheckDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assetCheck 实例对象
     * @return 实例对象
     */
    @Override
    public AssetCheck insert(AssetCheck assetCheck) {
        this.assetCheckDao.insert(assetCheck);
        return assetCheck;
    }

    /**
     * 修改数据
     *
     * @param assetCheck 实例对象
     * @return 实例对象
     */
    @Override
    public AssetCheck update(AssetCheck assetCheck) {
        this.assetCheckDao.update(assetCheck);
        return this.queryById(assetCheck.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.assetCheckDao.deleteById(id) > 0;
    }

    @Override
    public List<AssetCheck> queryAll(AssetCheck assetCheck) {
        return this.assetCheckDao.queryAll(assetCheck);
    }

    @Override
    public List<AssetCheck> search(AssetCheck assetCheck) {
        return this.assetCheckDao.search(assetCheck);
    }
}