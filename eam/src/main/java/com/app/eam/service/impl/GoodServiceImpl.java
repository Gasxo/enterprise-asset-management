package com.app.eam.service.impl;

import com.app.eam.dao.GoodDao;
import com.app.eam.entity.Good;
import com.app.eam.service.GoodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Good)表服务实现类
 *
 * @author makejava
 * @since 2020-03-29 11:15:32
 */
@Service("goodService")
public class GoodServiceImpl implements GoodService {
    @Resource
    private GoodDao goodDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Good queryById(Integer id) {
        return this.goodDao.queryById(id);
    }

    @Override
    public List<Good> search(Good good) {
        return this.goodDao.search(good);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Good> queryAllByLimit(int offset, int limit) {
        return this.goodDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    @Override
    public Good insert(Good good) {
        this.goodDao.insert(good);
        return good;
    }

    /**
     * 修改数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    @Override
    public Good update(Good good) {
        this.goodDao.update(good);
        return this.queryById(good.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.goodDao.deleteById(id) > 0;
    }

    @Override
    public List<Good> queryAll(Good good) {
        return this.goodDao.queryAll(good);
    }
}