package com.app.eam.service.impl;

import com.app.eam.dao.OrangetreeDao;
import com.app.eam.dao.UserDao;
import com.app.eam.entity.Orangetree;
import com.app.eam.entity.User;
import com.app.eam.service.OrangetreeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Orangetree)表服务实现类
 *
 * @author makejava
 * @since 2020-03-26 17:54:20
 */
@Service("orangetreeService")
public class OrangetreeServiceImpl implements OrangetreeService {
    @Resource
    private OrangetreeDao orangetreeDao;
    @Resource
    private UserDao userDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Orangetree queryById(Integer id) {
        return this.orangetreeDao.queryById(id);
    }

    @Override
    public List<Orangetree> queryAll(Orangetree orangetree) {
        return this.orangetreeDao.queryAll(orangetree);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Orangetree> queryAllByLimit(int offset, int limit) {
        return this.orangetreeDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param orangetree 实例对象
     * @return 实例对象
     */
    @Override
    public Orangetree insert(Orangetree orangetree) {
        this.orangetreeDao.insert(orangetree);
        return orangetree;
    }

    /**
     * 修改数据
     *
     * @param orangetree 实例对象
     * @return 实例对象
     */
    @Override
    public Orangetree update(Orangetree orangetree) {
        this.orangetreeDao.update(orangetree);
        return this.queryById(orangetree.getId());
    }

    @Override
    public void updateChild(Orangetree orangetree) {
        Orangetree findobj = new Orangetree();
        findobj.setFather(orangetree.getId());
        orangetree.setChildren(this.orangetreeDao.queryAll(findobj));
        if(orangetree.getChildren().size()>0){
            for (Orangetree child : orangetree.getChildren()) {
                child.setLevel(child.getLevel()+1);
                update(child);
                updateChild(child);
            }
        }
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.orangetreeDao.deleteById(id) > 0;
    }


    private void treeChild(Orangetree father, int maxFather) {
        Orangetree findObj = new Orangetree();
        User findUser = new User();
        findUser.setOrangeid(father.getId());
        father.setUsernum(userDao.queryAll(findUser).size());
        findObj.setFather(father.getId());
        father.setChildren(this.orangetreeDao.queryAll(findObj));
        for (Orangetree child : father.getChildren()) {
            child.setFathername(father.getName());
            if (child.getId() <= maxFather) {
                treeChild(child, maxFather);
            }else{
                findUser.setOrangeid(child.getId());
                child.setUsernum(userDao.queryAll(findUser).size());
            }
        }
    }

    @Override
    public List<Orangetree> treelist() {
        Orangetree findObj = new Orangetree();
        findObj.setLevel(1);
        List<Orangetree> list = this.orangetreeDao.queryAll(findObj);
        int maxFather = this.orangetreeDao.maxFather();
        for (Orangetree orangetree : list) {
            orangetree.setFathername("");
            treeChild(orangetree, maxFather);
        }
        return list;
    }


}