package com.app.eam.service;

import com.app.eam.entity.GoodClass;

import java.util.List;

/**
 * (GoodClass)表服务接口
 *
 * @author makejava
 * @since 2020-03-29 10:58:47
 */
public interface GoodClassService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GoodClass queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<GoodClass> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param goodClass 实例对象
     * @return 实例对象
     */
    GoodClass insert(GoodClass goodClass);

    /**
     * 修改数据
     *
     * @param goodClass 实例对象
     * @return 实例对象
     */
    GoodClass update(GoodClass goodClass);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    List<GoodClass> treelist();
    List<GoodClass> queryAll(GoodClass orangetree);
    void updateChild(GoodClass goodClass);

}