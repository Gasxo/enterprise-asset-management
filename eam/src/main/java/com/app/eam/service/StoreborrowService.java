package com.app.eam.service;

import com.app.eam.entity.Storeborrow;
import java.util.List;

/**
 * (Storeborrow)表服务接口
 *
 * @author makejava
 * @since 2020-04-04 14:11:20
 */
public interface StoreborrowService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Storeborrow queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Storeborrow> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param storeborrow 实例对象
     * @return 实例对象
     */
    Storeborrow insert(Storeborrow storeborrow);

    /**
     * 修改数据
     *
     * @param storeborrow 实例对象
     * @return 实例对象
     */
    Storeborrow update(Storeborrow storeborrow);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    List<Storeborrow> queryAll(Storeborrow storeborrow);
    List<Storeborrow> search(Storeborrow storeborrow);
}