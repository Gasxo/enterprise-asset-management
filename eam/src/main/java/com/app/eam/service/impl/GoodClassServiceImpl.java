package com.app.eam.service.impl;

import com.app.eam.dao.GoodClassDao;
import com.app.eam.dao.GoodDao;
import com.app.eam.entity.Good;
import com.app.eam.entity.GoodClass;
import com.app.eam.service.GoodClassService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (GoodClass)表服务实现类
 *
 * @author makejava
 * @since 2020-03-29 10:58:47
 */
@Service("goodClassService")
public class GoodClassServiceImpl implements GoodClassService {
    @Resource
    private GoodClassDao goodClassDao;

    @Resource
    private GoodDao goodDao;


    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public GoodClass queryById(Integer id) {
        return this.goodClassDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<GoodClass> queryAllByLimit(int offset, int limit) {
        return this.goodClassDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param goodClass 实例对象
     * @return 实例对象
     */
    @Override
    public GoodClass insert(GoodClass goodClass) {
        this.goodClassDao.insert(goodClass);
        return goodClass;
    }

    /**
     * 修改数据
     *
     * @param goodClass 实例对象
     * @return 实例对象
     */
    @Override
    public GoodClass update(GoodClass goodClass) {
        this.goodClassDao.update(goodClass);
        return this.queryById(goodClass.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.goodClassDao.deleteById(id) > 0;
    }
    private void treeChild(GoodClass father,int maxFather){
        GoodClass findobj = new GoodClass();
        Good findGood = new Good();
        findGood.setClassid(father.getId());
        father.setGoodsnum(goodDao.queryAll(findGood).size());
        findobj.setFather(father.getId());
        father.setChildren(this.goodClassDao.queryAll(findobj));
        for (GoodClass child : father.getChildren()) {
            child.setFathername(father.getName());
            if(child.getId() <= maxFather){
                treeChild(child,maxFather);
            }else{
                findGood.setClassid(child.getId());
                child.setGoodsnum(this.goodDao.queryAll(findGood).size());
            }
        }
    }
    @Override
    public List<GoodClass> treelist() {
        GoodClass findobj = new GoodClass();
        findobj.setLevel(1);
        List<GoodClass> list = this.goodClassDao.queryAll(findobj);
        int maxFather = this.goodClassDao.maxFather();
        for (GoodClass goodClass : list) {
            goodClass.setFathername("");
            treeChild(goodClass,maxFather);
        }
        return list;
    }

    @Override
    public List<GoodClass> queryAll(GoodClass goodClass) {
        return this.goodClassDao.queryAll(goodClass);
    }

    @Override
    public void updateChild(GoodClass goodClass) {
        GoodClass findobj = new GoodClass();
        findobj.setFather(goodClass.getId());
        goodClass.setChildren(this.goodClassDao.queryAll(findobj));
        if(goodClass.getChildren().size()>0){
            for (GoodClass child : goodClass.getChildren()) {
                child.setLevel(child.getLevel()+1);
                update(child);
                updateChild(child);
            }
        }
    }
}