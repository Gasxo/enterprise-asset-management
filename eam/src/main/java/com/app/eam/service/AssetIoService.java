package com.app.eam.service;

import com.app.eam.entity.AssetIo;
import java.util.List;

/**
 * (AssetIo)表服务接口
 *
 * @author makejava
 * @since 2020-04-11 10:35:31
 */
public interface AssetIoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetIo queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<AssetIo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assetIo 实例对象
     * @return 实例对象
     */
    AssetIo insert(AssetIo assetIo);

    /**
     * 修改数据
     *
     * @param assetIo 实例对象
     * @return 实例对象
     */
    AssetIo update(AssetIo assetIo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    List<AssetIo> queryAll(AssetIo assetIo);
}