package com.app.eam.service.impl;

import com.app.eam.dao.BorrowDetailDao;
import com.app.eam.entity.BorrowDetail;
import com.app.eam.service.BorrowDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (BorrowDetail)表服务实现类
 *
 * @author makejava
 * @since 2020-04-02 17:02:50
 */
@Service("borrowDetailService")
public class BorrowDetailServiceImpl implements BorrowDetailService {
    @Resource
    private BorrowDetailDao borrowDetailDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public BorrowDetail queryById(Integer id) {
        return this.borrowDetailDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<BorrowDetail> queryAllByLimit(int offset, int limit) {
        return this.borrowDetailDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param borrowDetail 实例对象
     * @return 实例对象
     */
    @Override
    public BorrowDetail insert(BorrowDetail borrowDetail) {
        this.borrowDetailDao.insert(borrowDetail);
        return borrowDetail;
    }

    /**
     * 修改数据
     *
     * @param borrowDetail 实例对象
     * @return 实例对象
     */
    @Override
    public BorrowDetail update(BorrowDetail borrowDetail) {
        this.borrowDetailDao.update(borrowDetail);
        return this.queryById(borrowDetail.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.borrowDetailDao.deleteById(id) > 0;
    }

    @Override
    public boolean deleteByRefId(Integer id) {
        return this.borrowDetailDao.deleteByRefId(id) >0;
    }

    @Override
    public List<BorrowDetail> queryAll(BorrowDetail borrowDetail) {
        return this.borrowDetailDao.queryAll(borrowDetail);
    }
}