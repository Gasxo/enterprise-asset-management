package com.app.eam.service.impl;

import com.app.eam.dao.OrderdetailDao;
import com.app.eam.entity.Orderdetail;
import com.app.eam.service.OrderdetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Orderdetail)表服务实现类
 *
 * @author makejava
 * @since 2020-03-31 15:18:14
 */
@Service("orderdetailService")
public class OrderdetailServiceImpl implements OrderdetailService {
    @Resource
    private OrderdetailDao orderdetailDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Orderdetail queryById(Integer id) {
        return this.orderdetailDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Orderdetail> queryAllByLimit(int offset, int limit) {
        return this.orderdetailDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param orderdetail 实例对象
     * @return 实例对象
     */
    @Override
    public Orderdetail insert(Orderdetail orderdetail) {
        this.orderdetailDao.insert(orderdetail);
        return orderdetail;
    }

    /**
     * 修改数据
     *
     * @param orderdetail 实例对象
     * @return 实例对象
     */
    @Override
    public Orderdetail update(Orderdetail orderdetail) {
        this.orderdetailDao.update(orderdetail);
        return this.queryById(orderdetail.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.orderdetailDao.deleteById(id) > 0;
    }

    @Override
    public boolean deleteByOrderid(Integer id) {
        return this.orderdetailDao.deleteByOrderId(id) > 0;
    }

    @Override
    public List<Orderdetail> queryAll(Orderdetail orderdetail) {
        return this.orderdetailDao.queryAll(orderdetail);
    }
}