package com.app.eam.service;

import com.app.eam.entity.AssetTransDetail;
import java.util.List;

/**
 * (AssetTransDetail)表服务接口
 *
 * @author makejava
 * @since 2020-04-15 18:29:56
 */
public interface AssetTransDetailService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetTransDetail queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<AssetTransDetail> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assetTransDetail 实例对象
     * @return 实例对象
     */
    AssetTransDetail insert(AssetTransDetail assetTransDetail);

    /**
     * 修改数据
     *
     * @param assetTransDetail 实例对象
     * @return 实例对象
     */
    AssetTransDetail update(AssetTransDetail assetTransDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    boolean deleteByTransId(Integer transid);
    List<AssetTransDetail> queryAll(AssetTransDetail assetTransDetail);

}