package com.app.eam.service.impl;

import com.app.eam.entity.AssetCheckDetail;
import com.app.eam.dao.AssetCheckDetailDao;
import com.app.eam.service.AssetCheckDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetCheckDetail)表服务实现类
 *
 * @author makejava
 * @since 2020-04-11 16:59:55
 */
@Service("assetCheckDetailService")
public class AssetCheckDetailServiceImpl implements AssetCheckDetailService {
    @Resource
    private AssetCheckDetailDao assetCheckDetailDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssetCheckDetail queryById(Integer id) {
        return this.assetCheckDetailDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<AssetCheckDetail> queryAllByLimit(int offset, int limit) {
        return this.assetCheckDetailDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assetCheckDetail 实例对象
     * @return 实例对象
     */
    @Override
    public AssetCheckDetail insert(AssetCheckDetail assetCheckDetail) {
        this.assetCheckDetailDao.insert(assetCheckDetail);
        return assetCheckDetail;
    }

    /**
     * 修改数据
     *
     * @param assetCheckDetail 实例对象
     * @return 实例对象
     */
    @Override
    public AssetCheckDetail update(AssetCheckDetail assetCheckDetail) {
        this.assetCheckDetailDao.update(assetCheckDetail);
        return this.queryById(assetCheckDetail.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.assetCheckDetailDao.deleteById(id) > 0;
    }

    @Override
    public boolean deleteBycheckId(Integer id) {
        return this.assetCheckDetailDao.deleteBycheckId(id) > 0;
    }

    @Override
    public List<AssetCheckDetail> queryAll(AssetCheckDetail assetCheckDetail) {
        return this.assetCheckDetailDao.queryAll(assetCheckDetail);
    }
}