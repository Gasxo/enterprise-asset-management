package com.app.eam.service;

import com.app.eam.entity.Orderdetail;

import java.util.List;

/**
 * (Orderdetail)表服务接口
 *
 * @author makejava
 * @since 2020-03-31 15:18:13
 */
public interface OrderdetailService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Orderdetail queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Orderdetail> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param orderdetail 实例对象
     * @return 实例对象
     */
    Orderdetail insert(Orderdetail orderdetail);

    /**
     * 修改数据
     *
     * @param orderdetail 实例对象
     * @return 实例对象
     */
    Orderdetail update(Orderdetail orderdetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    boolean deleteByOrderid(Integer id);
    List<Orderdetail> queryAll(Orderdetail orderdetail);

}