package com.app.eam.service.impl;

import com.app.eam.entity.AssetDetail;
import com.app.eam.dao.AssetDetailDao;
import com.app.eam.service.AssetDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetDetail)表服务实现类
 *
 * @author makejava
 * @since 2020-04-11 11:11:25
 */
@Service("assetDetailService")
public class AssetDetailServiceImpl implements AssetDetailService {
    @Resource
    private AssetDetailDao assetDetailDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssetDetail queryById(Integer id) {
        return this.assetDetailDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<AssetDetail> queryAllByLimit(int offset, int limit) {
        return this.assetDetailDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assetDetail 实例对象
     * @return 实例对象
     */
    @Override
    public AssetDetail insert(AssetDetail assetDetail) {
        this.assetDetailDao.insert(assetDetail);
        return assetDetail;
    }

    /**
     * 修改数据
     *
     * @param assetDetail 实例对象
     * @return 实例对象
     */
    @Override
    public AssetDetail update(AssetDetail assetDetail) {
        this.assetDetailDao.update(assetDetail);
        return this.queryById(assetDetail.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.assetDetailDao.deleteById(id) > 0;
    }

    @Override
    public List<AssetDetail> queryAll(AssetDetail assetDetail) {
        return this.assetDetailDao.queryAll(assetDetail);
    }
}