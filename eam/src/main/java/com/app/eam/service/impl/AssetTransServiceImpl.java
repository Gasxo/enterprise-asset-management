package com.app.eam.service.impl;

import com.app.eam.entity.AssetTrans;
import com.app.eam.dao.AssetTransDao;
import com.app.eam.service.AssetTransService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetTrans)表服务实现类
 *
 * @author makejava
 * @since 2020-04-11 12:53:47
 */
@Service("assetTransService")
public class AssetTransServiceImpl implements AssetTransService {
    @Resource
    private AssetTransDao assetTransDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssetTrans queryById(Integer id) {
        return this.assetTransDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<AssetTrans> queryAllByLimit(int offset, int limit) {
        return this.assetTransDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assetTrans 实例对象
     * @return 实例对象
     */
    @Override
    public AssetTrans insert(AssetTrans assetTrans) {
        this.assetTransDao.insert(assetTrans);
        return assetTrans;
    }

    /**
     * 修改数据
     *
     * @param assetTrans 实例对象
     * @return 实例对象
     */
    @Override
    public AssetTrans update(AssetTrans assetTrans) {
        this.assetTransDao.update(assetTrans);
        return this.queryById(assetTrans.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.assetTransDao.deleteById(id) > 0;
    }

    @Override
    public List<AssetTrans> queryAll(AssetTrans assetTrans) {
        return this.assetTransDao.queryAll(assetTrans);
    }

    @Override
    public List<AssetTrans> search(AssetTrans assetTrans) {
        return this.assetTransDao.search(assetTrans);
    }
}