package com.app.eam.service;

import com.app.eam.entity.AssetDetail;
import java.util.List;

/**
 * (AssetDetail)表服务接口
 *
 * @author makejava
 * @since 2020-04-11 11:11:24
 */
public interface AssetDetailService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetDetail queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<AssetDetail> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assetDetail 实例对象
     * @return 实例对象
     */
    AssetDetail insert(AssetDetail assetDetail);

    /**
     * 修改数据
     *
     * @param assetDetail 实例对象
     * @return 实例对象
     */
    AssetDetail update(AssetDetail assetDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<AssetDetail> queryAll(AssetDetail assetDetail);
}