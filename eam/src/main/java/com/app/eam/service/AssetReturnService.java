package com.app.eam.service;

import com.app.eam.entity.AssetReturn;
import java.util.List;

/**
 * (AssetReturn)表服务接口
 *
 * @author makejava
 * @since 2020-04-10 22:22:42
 */
public interface AssetReturnService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetReturn queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<AssetReturn> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assetReturn 实例对象
     * @return 实例对象
     */
    AssetReturn insert(AssetReturn assetReturn);

    /**
     * 修改数据
     *
     * @param assetReturn 实例对象
     * @return 实例对象
     */
    AssetReturn update(AssetReturn assetReturn);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    List<AssetReturn> queryAll(AssetReturn assetReturn);
    List<AssetReturn> search(AssetReturn assetReturn);

}