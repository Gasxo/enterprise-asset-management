package com.app.eam.service;

import com.app.eam.entity.SupplierClass;

import java.util.List;

/**
 * (SupplierClass)表服务接口
 *
 * @author makejava
 * @since 2020-03-29 20:36:29
 */
public interface SupplierClassService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SupplierClass queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<SupplierClass> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param supplierClass 实例对象
     * @return 实例对象
     */
    SupplierClass insert(SupplierClass supplierClass);

    /**
     * 修改数据
     *
     * @param supplierClass 实例对象
     * @return 实例对象
     */
    SupplierClass update(SupplierClass supplierClass);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<SupplierClass> queryAll(SupplierClass supplierClass);

}