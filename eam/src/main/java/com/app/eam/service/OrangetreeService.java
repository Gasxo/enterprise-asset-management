package com.app.eam.service;

import com.app.eam.entity.Orangetree;

import java.util.List;

/**
 * (Orangetree)表服务接口
 *
 * @author makejava
 * @since 2020-03-26 17:54:19
 */
public interface OrangetreeService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Orangetree queryById(Integer id);
    List<Orangetree> queryAll(Orangetree orangetree);
    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Orangetree> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param orangetree 实例对象
     * @return 实例对象
     */
    Orangetree insert(Orangetree orangetree);

    /**
     * 修改数据
     *
     * @param orangetree 实例对象
     * @return 实例对象
     */
    Orangetree update(Orangetree orangetree);
    void updateChild(Orangetree orangetree);
    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    List<Orangetree> treelist();


}