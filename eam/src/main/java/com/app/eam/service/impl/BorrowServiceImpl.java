package com.app.eam.service.impl;

import com.app.eam.dao.BorrowDao;
import com.app.eam.entity.Borrow;
import com.app.eam.service.BorrowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Borrow)表服务实现类
 *
 * @author makejava
 * @since 2020-04-02 16:56:24
 */
@Service("borrowService")
public class BorrowServiceImpl implements BorrowService {
    @Resource
    private BorrowDao borrowDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Borrow queryById(Integer id) {
        return this.borrowDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Borrow> queryAllByLimit(int offset, int limit) {
        return this.borrowDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param borrow 实例对象
     * @return 实例对象
     */
    @Override
    public Borrow insert(Borrow borrow) {
        this.borrowDao.insert(borrow);
        return borrow;
    }

    /**
     * 修改数据
     *
     * @param borrow 实例对象
     * @return 实例对象
     */
    @Override
    public Borrow update(Borrow borrow) {
        this.borrowDao.update(borrow);
        return this.queryById(borrow.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.borrowDao.deleteById(id) > 0;
    }

    @Override
    public List<Borrow> queryAll(Borrow borrow) {
        return this.borrowDao.queryAll(borrow);
    }

    @Override
    public List<Borrow> search(Borrow borrow) {
        return this.borrowDao.search(borrow);
    }
}