package com.app.eam.service;

import com.app.eam.entity.Good;

import java.util.List;

/**
 * (Good)表服务接口
 *
 * @author makejava
 * @since 2020-03-29 11:15:32
 */
public interface GoodService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Good queryById(Integer id);
    List<Good> search(Good good);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Good> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    Good insert(Good good);

    /**
     * 修改数据
     *
     * @param good 实例对象
     * @return 实例对象
     */
    Good update(Good good);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    List<Good> queryAll(Good orangetree);

}