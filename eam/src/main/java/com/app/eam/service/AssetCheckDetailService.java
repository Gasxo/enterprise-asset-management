package com.app.eam.service;

import com.app.eam.entity.AssetCheckDetail;
import java.util.List;

/**
 * (AssetCheckDetail)表服务接口
 *
 * @author makejava
 * @since 2020-04-11 16:59:55
 */
public interface AssetCheckDetailService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetCheckDetail queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<AssetCheckDetail> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param assetCheckDetail 实例对象
     * @return 实例对象
     */
    AssetCheckDetail insert(AssetCheckDetail assetCheckDetail);

    /**
     * 修改数据
     *
     * @param assetCheckDetail 实例对象
     * @return 实例对象
     */
    AssetCheckDetail update(AssetCheckDetail assetCheckDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);
    boolean deleteBycheckId(Integer id);
    List<AssetCheckDetail> queryAll(AssetCheckDetail assetCheckDetail);
}