package com.app.eam.service.impl;

import com.app.eam.dao.PurchaseenterlistDao;
import com.app.eam.entity.Purchaseenterlist;
import com.app.eam.service.PurchaseenterlistService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Purchaseenterlist)表服务实现类
 *
 * @author makejava
 * @since 2020-04-01 22:34:26
 */
@Service("purchaseenterlistService")
public class PurchaseenterlistServiceImpl implements PurchaseenterlistService {
    @Resource
    private PurchaseenterlistDao purchaseenterlistDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Purchaseenterlist queryById(Integer id) {
        return this.purchaseenterlistDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Purchaseenterlist> queryAllByLimit(int offset, int limit) {
        return this.purchaseenterlistDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param purchaseenterlist 实例对象
     * @return 实例对象
     */
    @Override
    public Purchaseenterlist insert(Purchaseenterlist purchaseenterlist) {
        this.purchaseenterlistDao.insert(purchaseenterlist);
        return purchaseenterlist;
    }

    /**
     * 修改数据
     *
     * @param purchaseenterlist 实例对象
     * @return 实例对象
     */
    @Override
    public Purchaseenterlist update(Purchaseenterlist purchaseenterlist) {
        this.purchaseenterlistDao.update(purchaseenterlist);
        return this.queryById(purchaseenterlist.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.purchaseenterlistDao.deleteById(id) > 0;
    }

    @Override
    public List<Purchaseenterlist> queryAll(Purchaseenterlist purchaseenterlist) {
        return this.purchaseenterlistDao.queryAll(purchaseenterlist);
    }
}