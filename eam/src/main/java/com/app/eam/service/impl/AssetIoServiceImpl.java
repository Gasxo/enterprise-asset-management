package com.app.eam.service.impl;

import com.app.eam.entity.AssetIo;
import com.app.eam.dao.AssetIoDao;
import com.app.eam.service.AssetIoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetIo)表服务实现类
 *
 * @author makejava
 * @since 2020-04-11 10:35:32
 */
@Service("assetIoService")
public class AssetIoServiceImpl implements AssetIoService {
    @Resource
    private AssetIoDao assetIoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssetIo queryById(Integer id) {
        return this.assetIoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<AssetIo> queryAllByLimit(int offset, int limit) {
        return this.assetIoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assetIo 实例对象
     * @return 实例对象
     */
    @Override
    public AssetIo insert(AssetIo assetIo) {
        this.assetIoDao.insert(assetIo);
        return assetIo;
    }

    /**
     * 修改数据
     *
     * @param assetIo 实例对象
     * @return 实例对象
     */
    @Override
    public AssetIo update(AssetIo assetIo) {
        this.assetIoDao.update(assetIo);
        return this.queryById(assetIo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.assetIoDao.deleteById(id) > 0;
    }

    @Override
    public List<AssetIo> queryAll(AssetIo assetIo) {
        return this.assetIoDao.queryAll(assetIo);
    }
}