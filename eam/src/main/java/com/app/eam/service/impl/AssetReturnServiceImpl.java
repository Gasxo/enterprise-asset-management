package com.app.eam.service.impl;

import com.app.eam.entity.AssetReturn;
import com.app.eam.dao.AssetReturnDao;
import com.app.eam.service.AssetReturnService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetReturn)表服务实现类
 *
 * @author makejava
 * @since 2020-04-10 22:22:42
 */
@Service("assetReturnService")
public class AssetReturnServiceImpl implements AssetReturnService {
    @Resource
    private AssetReturnDao assetReturnDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssetReturn queryById(Integer id) {
        return this.assetReturnDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<AssetReturn> queryAllByLimit(int offset, int limit) {
        return this.assetReturnDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assetReturn 实例对象
     * @return 实例对象
     */
    @Override
    public AssetReturn insert(AssetReturn assetReturn) {
        this.assetReturnDao.insert(assetReturn);
        return assetReturn;
    }

    /**
     * 修改数据
     *
     * @param assetReturn 实例对象
     * @return 实例对象
     */
    @Override
    public AssetReturn update(AssetReturn assetReturn) {
        this.assetReturnDao.update(assetReturn);
        return this.queryById(assetReturn.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.assetReturnDao.deleteById(id) > 0;
    }

    @Override
    public List<AssetReturn> queryAll(AssetReturn assetReturn) {
        return this.assetReturnDao.queryAll(assetReturn);
    }

    @Override
    public List<AssetReturn> search(AssetReturn assetReturn) {
        return this.assetReturnDao.search(assetReturn);
    }
}