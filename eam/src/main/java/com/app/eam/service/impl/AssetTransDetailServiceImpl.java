package com.app.eam.service.impl;

import com.app.eam.entity.AssetTransDetail;
import com.app.eam.dao.AssetTransDetailDao;
import com.app.eam.service.AssetTransDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AssetTransDetail)表服务实现类
 *
 * @author makejava
 * @since 2020-04-15 18:29:56
 */
@Service("assetTransDetailService")
public class AssetTransDetailServiceImpl implements AssetTransDetailService {
    @Resource
    private AssetTransDetailDao assetTransDetailDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AssetTransDetail queryById(Integer id) {
        return this.assetTransDetailDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<AssetTransDetail> queryAllByLimit(int offset, int limit) {
        return this.assetTransDetailDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param assetTransDetail 实例对象
     * @return 实例对象
     */
    @Override
    public AssetTransDetail insert(AssetTransDetail assetTransDetail) {
        this.assetTransDetailDao.insert(assetTransDetail);
        return assetTransDetail;
    }

    /**
     * 修改数据
     *
     * @param assetTransDetail 实例对象
     * @return 实例对象
     */
    @Override
    public AssetTransDetail update(AssetTransDetail assetTransDetail) {
        this.assetTransDetailDao.update(assetTransDetail);
        return this.queryById(assetTransDetail.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.assetTransDetailDao.deleteById(id) > 0;
    }

    @Override
    public boolean deleteByTransId(Integer transid) {
        return this.assetTransDetailDao.deleteByTransId(transid) > 0;
    }

    @Override
    public List<AssetTransDetail> queryAll(AssetTransDetail assetTransDetail) {
        return this.assetTransDetailDao.queryAll(assetTransDetail);
    }
}