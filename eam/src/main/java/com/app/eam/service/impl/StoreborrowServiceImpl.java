package com.app.eam.service.impl;

import com.app.eam.entity.Storeborrow;
import com.app.eam.dao.StoreborrowDao;
import com.app.eam.service.StoreborrowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Storeborrow)表服务实现类
 *
 * @author makejava
 * @since 2020-04-04 14:11:20
 */
@Service("storeborrowService")
public class StoreborrowServiceImpl implements StoreborrowService {
    @Resource
    private StoreborrowDao storeborrowDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Storeborrow queryById(Integer id) {
        return this.storeborrowDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Storeborrow> queryAllByLimit(int offset, int limit) {
        return this.storeborrowDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param storeborrow 实例对象
     * @return 实例对象
     */
    @Override
    public Storeborrow insert(Storeborrow storeborrow) {
        this.storeborrowDao.insert(storeborrow);
        return storeborrow;
    }

    /**
     * 修改数据
     *
     * @param storeborrow 实例对象
     * @return 实例对象
     */
    @Override
    public Storeborrow update(Storeborrow storeborrow) {
        this.storeborrowDao.update(storeborrow);
        return this.queryById(storeborrow.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.storeborrowDao.deleteById(id) > 0;
    }

    @Override
    public List<Storeborrow> queryAll(Storeborrow storeborrow) {
        return this.storeborrowDao.queryAll(storeborrow);
    }

    @Override
    public List<Storeborrow> search(Storeborrow storeborrow) {
        return this.storeborrowDao.search(storeborrow);
    }
}