package com.app.eam.dao;

import com.app.eam.entity.AssetDetail;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (AssetDetail)表数据库访问层
 *
 * @author makejava
 * @since 2020-04-11 11:11:24
 */
public interface AssetDetailDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetDetail queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<AssetDetail> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param assetDetail 实例对象
     * @return 对象列表
     */
    List<AssetDetail> queryAll(AssetDetail assetDetail);

    /**
     * 新增数据
     *
     * @param assetDetail 实例对象
     * @return 影响行数
     */
    int insert(AssetDetail assetDetail);

    /**
     * 修改数据
     *
     * @param assetDetail 实例对象
     * @return 影响行数
     */
    int update(AssetDetail assetDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}