package com.app.eam.dao;

import com.app.eam.entity.Storeborrow;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Storeborrow)表数据库访问层
 *
 * @author makejava
 * @since 2020-04-04 14:11:20
 */
public interface StoreborrowDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Storeborrow queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Storeborrow> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param storeborrow 实例对象
     * @return 对象列表
     */
    List<Storeborrow> queryAll(Storeborrow storeborrow);

    /**
     * 新增数据
     *
     * @param storeborrow 实例对象
     * @return 影响行数
     */
    int insert(Storeborrow storeborrow);

    /**
     * 修改数据
     *
     * @param storeborrow 实例对象
     * @return 影响行数
     */
    int update(Storeborrow storeborrow);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);
    List<Storeborrow> search(Storeborrow storeborrow);
}