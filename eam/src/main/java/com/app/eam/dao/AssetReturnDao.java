package com.app.eam.dao;

import com.app.eam.entity.AssetReturn;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (AssetReturn)表数据库访问层
 *
 * @author makejava
 * @since 2020-04-10 22:22:42
 */
public interface AssetReturnDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetReturn queryById(Integer id);
    List<AssetReturn> search(AssetReturn assetReturn);
    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<AssetReturn> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param assetReturn 实例对象
     * @return 对象列表
     */
    List<AssetReturn> queryAll(AssetReturn assetReturn);

    /**
     * 新增数据
     *
     * @param assetReturn 实例对象
     * @return 影响行数
     */
    int insert(AssetReturn assetReturn);

    /**
     * 修改数据
     *
     * @param assetReturn 实例对象
     * @return 影响行数
     */
    int update(AssetReturn assetReturn);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}