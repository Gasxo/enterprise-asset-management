package com.app.eam.dao;

import com.app.eam.entity.Orderdetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Orderdetail)表数据库访问层
 *
 * @author makejava
 * @since 2020-03-31 15:18:13
 */
public interface OrderdetailDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Orderdetail queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Orderdetail> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param orderdetail 实例对象
     * @return 对象列表
     */
    List<Orderdetail> queryAll(Orderdetail orderdetail);

    /**
     * 新增数据
     *
     * @param orderdetail 实例对象
     * @return 影响行数
     */
    int insert(Orderdetail orderdetail);

    /**
     * 修改数据
     *
     * @param orderdetail 实例对象
     * @return 影响行数
     */
    int update(Orderdetail orderdetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);
    int deleteByOrderId(Integer id);

}