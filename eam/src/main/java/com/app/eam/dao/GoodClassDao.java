package com.app.eam.dao;

import com.app.eam.entity.GoodClass;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (GoodClass)表数据库访问层
 *
 * @author makejava
 * @since 2020-03-29 10:58:46
 */
public interface GoodClassDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    GoodClass queryById(Integer id);
    int maxFather();
    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<GoodClass> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param goodClass 实例对象
     * @return 对象列表
     */
    List<GoodClass> queryAll(GoodClass goodClass);

    /**
     * 新增数据
     *
     * @param goodClass 实例对象
     * @return 影响行数
     */
    int insert(GoodClass goodClass);

    /**
     * 修改数据
     *
     * @param goodClass 实例对象
     * @return 影响行数
     */
    int update(GoodClass goodClass);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);
    List<GoodClass> treelist();


}