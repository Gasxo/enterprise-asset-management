package com.app.eam.dao;

import com.app.eam.entity.AssetCheck;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (AssetCheck)表数据库访问层
 *
 * @author makejava
 * @since 2020-04-11 12:36:02
 */
public interface AssetCheckDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AssetCheck queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<AssetCheck> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param assetCheck 实例对象
     * @return 对象列表
     */
    List<AssetCheck> queryAll(AssetCheck assetCheck);
    List<AssetCheck> search(AssetCheck assetCheck);
    /**
     * 新增数据
     *
     * @param assetCheck 实例对象
     * @return 影响行数
     */
    int insert(AssetCheck assetCheck);

    /**
     * 修改数据
     *
     * @param assetCheck 实例对象
     * @return 影响行数
     */
    int update(AssetCheck assetCheck);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}