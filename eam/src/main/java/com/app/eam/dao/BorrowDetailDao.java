package com.app.eam.dao;

import com.app.eam.entity.BorrowDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (BorrowDetail)表数据库访问层
 *
 * @author makejava
 * @since 2020-04-02 17:02:49
 */
public interface BorrowDetailDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    BorrowDetail queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<BorrowDetail> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param borrowDetail 实例对象
     * @return 对象列表
     */
    List<BorrowDetail> queryAll(BorrowDetail borrowDetail);

    /**
     * 新增数据
     *
     * @param borrowDetail 实例对象
     * @return 影响行数
     */
    int insert(BorrowDetail borrowDetail);

    /**
     * 修改数据
     *
     * @param borrowDetail 实例对象
     * @return 影响行数
     */
    int update(BorrowDetail borrowDetail);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);
    int deleteByRefId(Integer id);

}